using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    
    public static ObjectPooler Instance;

    private void Awake()
    {
        Instance = this;
        InitPoolingDic();
    }

    public List<Pool> pools;
    private Dictionary<string, Queue<GameObject>> _poolDictionary;
    
    private void InitPoolingDic()
    {
        _poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (var pool in pools)
        {
            var objPool = new Queue<GameObject>();
            for (var i = 0; i < pool.size; i++)
            {
                var obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objPool.Enqueue(obj);
            }
            _poolDictionary.Add(pool.tag, objPool );
        }
    }

    public GameObject SpawnFromPool(string tag, Vector3 pos, Quaternion rot)
    {
        if (!_poolDictionary.ContainsKey(tag)) return null;
        var objToSpawn = _poolDictionary[tag].Dequeue();
        objToSpawn.transform.position = pos;
        objToSpawn.transform.rotation = rot;
        objToSpawn.SetActive(true);
        
        var pooledObj = objToSpawn.GetComponent<IPooledObj>();
        pooledObj?.OnObjSpawn();

        _poolDictionary[tag].Enqueue(objToSpawn);
        return objToSpawn;
    }

    public void BackToPool()
    {
        //TODO implement
    }
}
