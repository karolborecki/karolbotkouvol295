using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;

public class Tower : MonoBehaviour, IPooledObj
{
    public static int Count = 0;
    
    private bool _isActivated = false;

    public SpriteRenderer body;
    public SpriteRenderer barrel;
    
    public float activationTime = 6;
    public float turnTime = 0.5f;
    
    public Vector2Int turnAngels;
    public Vector2Int shootDistance;
    public int bullets = 12;

    public void OnObjSpawn()
    {
        InvokeRepeating("RandomTurn", turnTime, turnTime);
        
        Count++;
        UIManager.Instance.UpdatedText(Count);
        if (Count != 1) Invoke("Activate", activationTime);
        else Activate();
    }

    private void RandomTurn()
    {
        if (!_isActivated) return;
        var angel = RandomValueFromBetweenVector(turnAngels);
        Turn(angel);
        Shoot();
    }

    private void Turn(float degree)
    {
        var rotation = new Vector3(0, 0, (transform.eulerAngles.z + degree) % 360);
        transform.eulerAngles += rotation;
    }

    private void Shoot()
    {
        var bulletInstantiate = ObjectPooler.Instance.SpawnFromPool("bullet", transform.position, transform.rotation).GetComponent<Bullter>();
        var randomDistance = RandomValueFromBetweenVector(shootDistance);
        bulletInstantiate.Shoot(randomDistance);
        bullets--;
        if (bullets <= 0) Deactivate();
    }

    private static float RandomValueFromBetweenVector(Vector2Int v)
    {
        return Random.Range(v.x, v.y);
    }

    private void Activate()
    {
        _isActivated = true;
        body.color = Color.red;
        barrel.color = Color.red;
    }
    
    private void Deactivate()
    {
        _isActivated = false;
        body.color = Color.black;
        barrel.color = Color.black;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (!col.gameObject.CompareTag("Bullter")) return;
        //TODO Back to pool tower and bullet!
    }
}
