using DefaultNamespace;
using UnityEngine;

public class Bullter : MonoBehaviour, IPooledObj
{
    public int speed = 4;
    private float _distance = -1;
    private Vector2 _startingPoint;
    private Vector3 _dir;
    
    public void OnObjSpawn()
    {
        _startingPoint = transform.position;
        GetComponent<Collider2D>().enabled = true;
    }

    private void Update()
    {
        if(_distance <= 0)
            return;
        
        transform.position += (Vector3)(_dir * speed * Time.deltaTime);
        if (Vector2.Distance(_startingPoint, transform.position) >= _distance)
            ReachedDestination();
    }

    public void Shoot(float distance)
    {   
        _distance = distance;
        CalculateDir();
    }
    private void ReachedDestination()
    {
        HideBullet();
        SpawnNext();
        //TODO Back to pool bullet!
    }

    private void HideBullet()
    {
        gameObject.SetActive(false);
        _distance = -1;
    }

    private void SpawnNext()
    {
        if(Tower.Count < 100) ObjectPooler.Instance.SpawnFromPool("tower", transform.position, Quaternion.identity);
    }

    private void CalculateDir()
    {
        var vectorDir = Vector3.right;
        
        var angle = transform.eulerAngles.z * Mathf.Deg2Rad;
        var sin = Mathf.Sin(angle);
        var cos = Mathf.Cos(angle);
 
        _dir = new Vector3(vectorDir.x * cos - vectorDir.y * sin, vectorDir.x * sin + vectorDir.y * cos, 0f );
    }
}
