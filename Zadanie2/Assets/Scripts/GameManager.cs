using UnityEngine;

public class GameManager : MonoBehaviour
{
    private void Start()
    {
        ObjectPooler.Instance.SpawnFromPool("tower", transform.position, Quaternion.identity);
    }
}
