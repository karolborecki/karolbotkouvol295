using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public TextMeshProUGUI txt;

    public void UpdatedText(int count)
    {
        txt.text = "Towers: " + count.ToString();
    }
}
