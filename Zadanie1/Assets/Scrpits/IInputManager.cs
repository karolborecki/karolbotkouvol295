using System;
using UnityEngine;

namespace IInputManagerNamespace
{
    public interface IInputManager
    {
        void AddListenerOnMouseDownEvent(Action<Vector2> listener);
        void RemoveListenerOnMouseDownEvent(Action<Vector2> listener);

        void AddListenerOnMouseEvent(Action<Vector2> listener);

        void RemoveListenerOnMouseEvent(Action<Vector2> listener);

    }
}