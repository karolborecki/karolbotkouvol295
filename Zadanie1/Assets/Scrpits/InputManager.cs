﻿using System;
using IInputManagerNamespace;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour, IInputManager
{
    private Action<Vector2> _onMouseDownHandler;
    private Action<Vector2> _onMouseHandler;


    private void Update()
    {
        if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) return;

        if (Input.GetMouseButtonDown(0)) GetPointerDown();
        if (Input.GetMouseButton(0)) GetPointer();
    }

    private void GetPointerDown()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray.origin, ray.direction, out var hit, Mathf.Infinity)) return;
        _onMouseDownHandler?.Invoke(hit.point);
        Debug.Log(hit.point);
    }

    private void GetPointer()
    {
        var point= Camera.main.ScreenToWorldPoint(Input.mousePosition);
        _onMouseHandler?.Invoke(point);
        Debug.Log(point);
    }
    
    public void AddListenerOnMouseDownEvent(Action<Vector2> listener)
    {
        _onMouseDownHandler += listener;
    }

    public void RemoveListenerOnMouseDownEvent(Action<Vector2> listener)
    {
        _onMouseDownHandler -= listener;

    }
    
    public void AddListenerOnMouseEvent(Action<Vector2> listener)
    {
        _onMouseHandler += listener;
    }

    public void RemoveListenerOnMouseEvent(Action<Vector2> listener)
    {
        _onMouseHandler -= listener;
    }
    
    
}
