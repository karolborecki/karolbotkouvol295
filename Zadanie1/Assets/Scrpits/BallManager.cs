﻿using System.Data;
using System.Linq;
using IInputManagerNamespace;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallManager : MonoBehaviour
{
    private IInputManager _input;
    private Rigidbody2D _rb;
        
    private void Start()
    {
        _input = FindObjectsOfType<MonoBehaviour>().OfType<IInputManager>().FirstOrDefault() ?? 
                 Instantiate(new GameObject("IM"), Vector2.zero, Quaternion.identity).AddComponent<InputManager>() as InputManager;


        _rb = GetComponent<Rigidbody2D>();
        
        _input.AddListenerOnMouseEvent(HandleInput);
    }

    private void HandleInput(Vector2 pointerPos)
    {
        if(Vector3.Distance(pointerPos, transform.position) < transform.localScale.x)
            _rb.position = pointerPos;
    }

}
